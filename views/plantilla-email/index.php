<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Restablecer contraseña</title>
    <style>
        h1 {
            font-size: 40px;
            text-align: center;
        }

        p {
            font-size: 14px;
            color: black !important;
        }

        .btn {
            color: white !important;
            background: #0087ff;
            text-decoration: none;
            text-transform: uppercase;
            padding: .5rem 5rem;
            display: flex;
            justify-content: center;
            width: max-content;
            border-radius: 2rem;
            font-weight: 700;
            margin: 0 auto;
            max-width: 6rem;
        }

        .container {
            width: 100%;
            max-width: 480px;
            margin: 0 auto;
        }

        .logo-email {
            width: 50px !important;
        }

        .logo {
            display: flex;
            justify-content: center;
            width: 200px;
            height: 200px;
            margin: 0 auto;
        }

        .logo-pub {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="logo-container">
            <div class="logo">    
            <img src="https://i.ibb.co/b649Gh4/Logo-The-Geekend.png" class="logo-pub" alt="Logo The Geekend">
            </div>


        </div>
        <div class="info">
            <h1>Hola, <?php echo $nombreUsuario ?>:</h1>
            <p>
            ¿No puedes acceder a The Geekend? No te preocupes. Haz clic en el botón a continuación para restablecer tu contraseña.
            </p>

            <?= Html::a(
                'Restablecer contraseña',
                Yii::$app->urlManager->createAbsoluteUrl(['confirmar-password/reset-password', 'email' => $email]),
                ['class' => 'btn']
            )
            ?>
            <p>
                Al restablecer la contraseña, también confirmarás el correo asociado a tu cuenta.
            </p>
            <p>
                Si no has solicitado ningún restablecimiento, puedes ignorar este mensaje con total tranquilidad.
            </p>
            <p>
                <br <span>El equipo de The Geekend</span>
            </p>

        </div>
    </div>
</body>

</html>
