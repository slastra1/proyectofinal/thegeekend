<?php

use yii\helpers\Html;
use yii\web\View;
use app\models\Jugadores;

/** @var View $this */
/** @var Jugadores $jugador */

$this->title = 'Perfil del Usuario';
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css');
$this->registerCss("
.profile-container {
    text-align: center;
    margin-top: 20px; /* Reduce el espacio superior */
}
.profile-header {
    font-size: 24px;
    font-weight: bold;
}
.profile-icon {
    font-size: 50px; /* Reducir el tamaño del ícono */
    color: #007bff;
    vertical-align: middle; /* Alinear verticalmente con el texto */
    margin-right: 10px; /* Añadir margen derecho */
}
.profile-details {
    margin-top: 20px;
    font-size: 18px;
    text-align: left; /* Alinear el texto a la izquierda */
}
.profile-details ul {
    list-style-type: none; /* Quitar viñetas de la lista */
    padding: 0; /* Quitar el relleno de la lista */
}
.profile-details ul li {
    margin-bottom: 10px; /* Añadir espacio entre elementos de lista */
}
");

$jugador = Yii::$app->user->identity;

?>
<div class="profile-container">
    <!-- Bienvenido + Nick -->
    <div class="profile-header">
        <?= Html::encode('Bienvenido a tu perfil, ' . $jugador->nick) ?>
        <br></br>
    </div>
    <!-- Icono de perfil -->
    <i class="fas fa-user profile-icon"></i>
    <!-- Detalles del perfil -->
    <div class="profile-details">
    <br></br>
        <p><b>Aquí puedes ver los detalles de tu cuenta:</b></p>
        <ul>
            <br></br>
            <li><strong>Nombre:</strong> <?= Html::encode($jugador->nombre) ?></li>
            <li><strong>Nombre de usuario:</strong> <?= Html::encode($jugador->nick) ?></li>
            <li><strong>Email:</strong> <?= Html::encode($jugador->email) ?></li>
            <li><strong>D.N.I:</strong> <?= Html::encode($jugador->dni) ?></li>
            <li><strong>Nombre de Grupo:</strong> <?= Html::encode($jugador->nombregrupo) ?></li>
        </ul>
    </div>
</div>
