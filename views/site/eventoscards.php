<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\db\Query;

/** @var yii\web\View $this */

$this->title = 'Eventos';
$this->registerCssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css');
$this->registerJsFile('https://code.jquery.com/jquery-3.3.1.slim.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCss("
.card {
    opacity: 0;
    transform: translateY(20px);
    transition: opacity 0.5s ease, transform 0.8s ease;
}

.card.show {
    opacity: 1;
    transform: translateY(0);
}

.card:hover {
    transform: scale(1.02);
    transition: transform 0.8s ease, opacity 0.8s ease;
}
.card-body .actores {
    display: none;
}
.card:hover .actores {
    display: block;
}
.eventos-index .card {
    margin-left: 50px;
    margin-right: 50px;
    background-color: #DCD0FF;
}
.eventos-index .card {
    width: calc(80% - 30px); /* Calcular el ancho de las tarjetas para dos columnas */
}
.card-body li, .actores li {
    color: #e94196; /* Establece el color del texto de los actores */
}
.card-img-top {
    width: 100%; /* Asegura que la imagen ocupe todo el ancho de la tarjeta */
    height: 200px; /* Establece una altura fija para la imagen */
    object-fit: cover; /* Hace que la imagen se recorte para ajustarse al tamaño especificado */
}
");

// Variables para el filtro de meses y año
$meses = [
    '' => 'Seleccione mes', // Cambio realizado aquí
    '01' => 'Enero',
    '02' => 'Febrero',
    '03' => 'Marzo',
    '04' => 'Abril',
    '05' => 'Mayo',
    '06' => 'Junio',
    '07' => 'Julio',
    '08' => 'Agosto',
    '09' => 'Septiembre',
    '10' => 'Octubre',
    '11' => 'Noviembre',
    '12' => 'Diciembre',
];

$anios = range(date('Y'), 2000); // Desde el año actual hasta el año 2000

// Variables para almacenar los valores seleccionados del filtro
$selectedMes = isset($_GET['mes']) ? $_GET['mes'] : ''; // Cambio realizado aquí
$selectedAnio = isset($_GET['anio']) ? $_GET['anio'] : ''; // Cambio realizado aquí

// Consulta para obtener eventos con actores ordenados por fecha de la más reciente a la más vieja
$query = new Query();
$eventos = $query->select(['e.ideventos', 'e.nombre AS evento_nombre', 'e.lugar', 'e.finicio', 'e.ffinal', 'a.nombre AS actor_nombre', 'a.nombrepersonaje AS personaje'])
    ->from(['e' => 'eventos'])
    ->leftJoin(['a' => 'actores'], 'e.ideventos = a.idactores')
    ->leftJoin(['as' => 'asisten'], 'e.ideventos = as.ideventos');

// Aplicar filtro solo si se selecciona mes y año
if (!empty($selectedMes) && !empty($selectedAnio)) {
    $eventos->where(['MONTH(e.finicio)' => $selectedMes])
        ->andWhere(['YEAR(e.finicio)' => $selectedAnio]);
}

$eventos->orderBy(['e.finicio' => SORT_DESC]); 

$eventos = $eventos->all(Yii::$app->db);
?>

<div class="eventos-index">
    <h1 class="text-center" style="margin:25px 0;"> <?= Html::encode($this->title) ?> </h1> <!-- Ajustar el espacio entre el título y los filtros -->

    <!-- Filtro de meses y año -->
    <div class="container mb-4"> <!-- Colocar las tarjetas a 25px debajo de los filtros -->
        <form class="form-inline">
            <label class="mr-2">Mes:</label>
            <select class="form-control mr-2" name="mes">
                <?php foreach ($meses as $mesNumero => $nombreMes): ?>
                    <option value="<?= $mesNumero ?>" <?= $selectedMes == $mesNumero ? 'selected' : '' ?>><?= $nombreMes ?></option>
                <?php endforeach; ?>
            </select>
            <label class="mr-2">Año:</label>
            <select class="form-control mr-2" name="anio">
                <?php foreach ($anios as $anio): ?>
                    <option value="<?= $anio ?>" <?= $selectedAnio == $anio ? 'selected' : '' ?>><?= $anio ?></option>
                <?php endforeach; ?>
            </select>
            <button type="submit" class="btn btn-primary">Filtrar</button>
        </form>
    </div>

    <!-- Tarjetas de eventos -->
<div class="row"> <!-- Ajustar el espacio de las tarjetas -->
    <?php 
    $eventosAgrupados = [];
    foreach ($eventos as $evento) {
        $eventosAgrupados[$evento['ideventos']]['info'] = $evento;
        $eventosAgrupados[$evento['ideventos']]['actores'][] = [
            'nombre' => $evento['actor_nombre'],
            'personaje' => $evento['personaje'],
        ];
    }

    foreach ($eventosAgrupados as $evento): 
    ?>
        <div class="col-md-4 mb-4">
            <div class="card">
                <img src="<?= Url::to('@web/images/EventosCards/' . $evento['info']['lugar'] . '.jpeg') ?>" class="card-img-top" alt="<?= Html::encode($evento['info']['evento_nombre']) ?>">
                <div class="card-body">
                    <h5 class="card-title"><?= Html::encode($evento['info']['evento_nombre']) ?></h5>
                    <li class="card-text"><strong>Lugar:</strong> <?= Html::encode($evento['info']['lugar']) ?></li>
                    <li class="card-text"><strong>Duración:</strong> <?= Html::encode($evento['info']['finicio'] . ' - ' . $evento['info']['ffinal']) ?></li>
                    <br>
                    <div class="actores">
                        <h6>Actriz/Actor:</h6>
                        <ul>
                            <?php foreach ($evento['actores'] as $actor): ?>
                                <li><?= Html::encode($actor['nombre']) ?><br>(<?= Html::encode($actor['personaje']) ?>)</li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        let cards = document.querySelectorAll('.card');
        cards.forEach(function(card) {
            card.classList.add('show');
        });
    });
</script>
