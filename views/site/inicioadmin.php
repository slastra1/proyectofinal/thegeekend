<?php
use yii\helpers\Html;

$this->title = 'Administración - The Geekend';
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container" style="margin-top: 50px;">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header text-center">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
                <div class="card-body text-center">
                    <p>Bienvenido al panel de administración. Utilice el siguiente botón para crear un nuevo evento.</p>
                    <?= Html::a('Crear Evento', ['evento/create'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
