<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;
use yii\bootstrap4\Alert;

/** @var yii\web\View $this */
/** @var app\models\RegistroUsuarios $model */

AppAsset::register($this);

?>
<div class="container-principal crear-usuario w-100 h-100">
    <div class="container-create-user w-100 h-100">
        <div class="overlay"></div>
        <div class="row h-100 s-1 justify-content-center align-items-center animate__animated animate__fadeIn">
            <div class="col-8">
                <div class="form-register">
                    <h1 class="title">Registrar</h1>

                    <?php 
                    // Mostrar mensajes flash
                    foreach (Yii::$app->session->getAllFlashes() as $type => $message) {
                        echo Alert::widget([
                            'options' => ['class' => 'alert-success alert-dismissible fade show' . $type],
                            'body' => $message,
                        ]);
                    }
                    ?>

                    <?php $form = ActiveForm::begin(['options' => ['class form-group' => 'form']]); ?>

                    <?= $form->field($model, 'nombre')->textInput(['class' => 'form-control form-style', 'placeholder' => 'Tu nombre'])->label(false) ?>

                    <?= $form->field($model, 'nick')->textInput(['class' => 'form-control form-style', 'placeholder' => 'Tu nombre de usuario'])->label(false) ?>

                    <?= $form->field($model, 'email')->textInput(['class' => 'form-control form-style', 'placeholder' => 'Tu correo'])->label(false) ?>

                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-style', 'placeholder' => 'Tu contraseña'])->label(false) ?>

                    <?= $form->field($model, 'confirmarPassword')->passwordInput(['class' => 'form-control form-style', 'placeholder' => 'Confirma tu contraseña'])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Registrar', ['class' => 'btn btn-clean btn-principal w-100']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>

.alert-dismissible {
    position: absolute;
    top: 135px;
    right: 680px;
    z-index: 1000;
    width: 300px;
    height: 50px;
}

.alert-success {
    background-color: #d4edda;
    color: #155724;
    border-color: #c3e6cb;
}

.alert-dismissible .close {
    position: relative;
    top: -50px;
    right: -70px;
    color: inherit;
}
</style>