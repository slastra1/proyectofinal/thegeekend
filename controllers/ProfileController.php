<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Jugadores;
use yii\filters\AccessControl;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $jugador = Yii::$app->user->identity;

        if ($jugador->load(Yii::$app->request->post())) {
            Yii::info('Datos cargados desde la solicitud: ' . print_r(Yii::$app->request->post(), true));
            if ($jugador->save()) {
                Yii::$app->session->setFlash('success', 'Perfil actualizado correctamente.');
                return $this->refresh();
            } else {
                Yii::info('Error al intentar guardar los datos: ' . print_r($jugador->errors, true));
            }
        }

        return $this->render('index', [
            'jugador' => $jugador,
        ]);
    }

}
